import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/recherche',
    name: 'Recherche',
   
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/accueil',
    name: 'Accueil',
   
    component: () => import(/* webpackChunkName: "about" */ '../views/Accueil.vue')
  },
  {
    path: '/apropos',
    name: 'Apropos',
   
    component: () => import(/* webpackChunkName: "about" */ '../views/Apropos.vue')
  },
  {
    path: '/confidentialite',
    name: 'Confidentialite',
   
    component: () => import(/* webpackChunkName: "about" */ '../views/Confidentialite.vue')
  },
  {
    path: '/utilisation',
    name: 'Utilisation',
   
    component: () => import(/* webpackChunkName: "about" */ '../views/Utilisation.vue')
  },
  {
    path: '/detail',
    name: 'Detail',
   
    component: () => import(/* webpackChunkName: "about" */ '../views/Detail.vue')
  },
  {
    path: '/login',
    name: 'Login',
   
    component: () => import(/* webpackChunkName: "about" */ '../views/Login.vue')
  },
  {
    path: '/login_client',
    name: 'LoginClient',
   
    component: () => import(/* webpackChunkName: "about" */ '../views/LoginClient.vue')
  },
  {
    path: '/inscription',
    name: 'Inscription',
   
    component: () => import(/* webpackChunkName: "about" */ '../views/Inscription.vue')
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
   
    component: () => import(/* webpackChunkName: "about" */ '../views/Dashboard.vue')
  },
  {
    path: '/dashboard_client',
    name: 'DashboardClient',
   
    component: () => import(/* webpackChunkName: "about" */ '../views/Dashboardclient.vue')
  },
  {
    path: '/dashboard_list',
    name: 'Dashboard_list',
   
    component: () => import(/* webpackChunkName: "about" */ '../components/dashboard/List.vue')
  },
  {
    path: '/dashboard_reservation_list',
    name: 'Dashboard_reservation_list',
   
    component: () => import(/* webpackChunkName: "about" */ '../components/dashboard/reserve_mobile.vue')
  },
  {
    path: '/dashboard_users',
    name: 'Dashboard_users',
   
    component: () => import(/* webpackChunkName: "about" */ '../components/dashboard/Users.vue')
  },
  {
    path: '/dashboard_profile',
    name: 'Dashboard_profile',
   
    component: () => import(/* webpackChunkName: "about" */ '../components/dashboard/Profile.vue')
  },
  {
    path: '/dashboard-new',
    name: 'New',
   
    component: () => import(/* webpackChunkName: "about" */ '../components/dashboard/New.vue')
  },
  {
    path: '/dashboard-new-s',
    name: 'New_s',
   
    component: () => import(/* webpackChunkName: "about" */ '../components/dashboard/New_s.vue')
  },
  {
    path: '/recherche-mob',
    name: 'Recherchemob',
   
    component: () => import(/* webpackChunkName: "about" */ '../components/recherche/Mobile.vue')
  },
  { 
    path: '/dashboard-newm',
    name: 'New',
   
    component: () => import(/* webpackChunkName: "about" */ '../components/dashboard/New-mobile.vue')
  },
  {
    path: '/contact',
    name: 'Contact',
   
    component: () => import(/* webpackChunkName: "about" */ '../views/Contact.vue')
  },
  {
    path: '/image-zoom',
    name: 'Image',
   
    component: () => import(/* webpackChunkName: "about" */ '../components/detail/Image.vue')
  },
  {
    path: '/reservation',
    name: 'Reservation',
   
    component: () => import(/* webpackChunkName: "about" */ '../components/detail/Reservation.vue')
  },
  {
    path: '/wreservation',
    name: 'Wreservation',
   
    component: () => import(/* webpackChunkName: "about" */ '../components/detail/Wreservation.vue')
  },
  {
    path: '/partenariat',
    name: 'Partenariat',
   
    component: () => import(/* webpackChunkName: "about" */ '../views/Partenariat.vue')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
