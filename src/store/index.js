import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    users: {
      name : '',
      number: '',
      email : '',
      id : null,
      reservation : '',
      reservationj : '',
      visite : '',
      visitej : '',
      residence : '',
      user : '',
    },
    money: {
      nom: 'CFA',
      icone: 'payments',
      value: 1,
    },
    enLigne: true,
  },
  mutations: {
    Connexion: (state, data) => {
      state.users.name = data.nom;
      state.users.email = data.email;
      state.users.number = data.contact;
      state.users.id = data.n_user;
    },
    Devise: (state, data) => {
      state.money.nom = data.nom;
      state.money.icone = data.icone;
      state.money.value = data.value;
    },
  },
  actions: {
  },
  modules: {
  },
  plugins: [createPersistedState()]
})
