import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import * as VueGoogleMaps from 'vue2-google-maps'
import router from './router'
import store from './store'
import Vuesax from 'vuesax'
import ElementUI from 'element-ui'
import axios from 'axios';
import VueSimpleAlert from "vue-simple-alert";
import Cloudinary from 'cloudinary-vue';
import 'vuesax/dist/vuesax.css' //Vuesax styles
import 'material-icons/iconfont/material-icons.css';
import './assets/style.css'
import 'element-ui/lib/theme-chalk/index.css';
import 'sweetalert2/dist/sweetalert2.min.css';
import VueCarousel from 'vue-carousel';
import VueOffline from 'vue-offline'
import VueFbCustomerChat from 'vue-fb-customer-chat';
import VModal from 'vue-js-modal';
import VueGlide from 'vue-glide-js'
import 'vue-glide-js/dist/vue-glide.css'
import elementLangDe from 'element-ui/lib/locale/lang/fr';
import elementLocale from 'element-ui/lib/locale';
elementLocale.use(elementLangDe);
Vue.use(VueGlide)
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyAx1wko5GuMDG7y_d_eOqyewTJbY9VxLmQ',
    libraries: 'places', 
  },

})
Vue.use(VModal)
Vue.use(axios);
Vue.use(VueOffline)
Vue.use(VueCarousel);
Vue.use(Vuesax);
Vue.use(ElementUI);
Vue.use(VueSimpleAlert);
Vue.use(Cloudinary, {
  configuration: {
    cloudName: "r-sidence-meubl-e"
  }
});
Vue.use(VueFbCustomerChat, {
  page_id: 105027578764649, //  change 'null' to your Facebook Page ID,
  theme_color: '#333333', // theme color in HEX
  locale: 'en_US', // default 'en_US'
})
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
