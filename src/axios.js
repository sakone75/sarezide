import axios from 'axios';

var myApi = axios.create({
   baseURL: 'https://my-domain.com/api/',
   timeout: 1000,
   headers: {
      'X-Custom-Header': 'CustomHeader1'
   }
});

// another api service
var amazonApi = axios.create({
   baseURL: 'https://amazon-domain.com/api/',
   timeout: 2000,
   headers: {
      'X-Custom-Header': 'CustomHeader2'
   }
});

export default {
   myApi,
   amazonApi
}
// axios.get('/user/12345')
//  .catch(function (error) {
 //   if (error.response) {
      
//      console.log(error.response.data);
 //     console.log(error.response.status);
 //     console.log(error.response.headers);
 //   } else if (error.request) {
      
//      console.log(error.request);
//    } else {
     
 //     console.log('Error', error.message);
//    }
//    console.log(error.config);
//  });